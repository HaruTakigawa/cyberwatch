﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivate : Abilities
{
    //[SerializeField] private float castRadius;
    private Collider2D[] targetsInRadius;
    private Vector3 mousePos;
    private LayerMask guardMask;
    [SerializeField] private CastRadius castRadius;

    public List<GameObject> guardsInRadius = new List<GameObject>();
    public float deactivateRadiusSize;

    public CastRangeRenderer CastRangeRenderer;

    public bool isDeactivating;


    public void Start()
    {
        isDeactivating = false;
    }

    private void Update()
    {
        CastRangeRenderer.SetOrigin(transform.position);
        CastRangeRenderer.SetAimDirection(transform.forward);

        if (isUsed)
            Cooldown();

        if (playerAbilities.hasSelectedDeactivateGuard)
        {
            DeactivateEnemies();
            castRadius.radiusSize = deactivateRadiusSize;
        }

        castRangeCollider.radius = castRange;
    }

    void DeactivateEnemies()
    {
        endPoint.SetActive(true);
        mousePos = Input.mousePosition;
        mousePos.z = 1;
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
        endPoint.transform.position = worldPosition;

        Vector3 dir = endPoint.transform.position - transform.position;
        float dis = dir.magnitude;

        if (dis > castRange) return;
        if (!playerAbilities.hasSelectedDeactivateGuard) return;


        float angle = AngleBetweenTwoPoints(player.transform.position, worldPosition);
        player.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle - 90));

        if (!isUsed)
        {
            if (!isInRange)
            {
                return;
            }

            if (Input.GetMouseButton(0))
            {
                isDeactivating = true;
                //targetsInRadius = Physics2D.OverlapCircleAll(mousePos, castRadius.radiusSize, guardMask);
                castRadius.CheckForEnemies();

                if (vfx != null)
                    vfx.SetActive(true);

                foreach (GameObject target in guardsInRadius)
                {
                    target.GetComponent<DeactivatedState>().timeLeft = duration;
                    target.GetComponent<GuardsBehaviour>().guardState = GuardState.Deactivated;
                    
                }

                playerAbilities.hasSelectedDeactivateGuard = false;
                isUsed = true;
                endPoint.SetActive(false);

                if (isDeactivating)
                    StartCoroutine(DeactivateDelay());
            }
        }
        if (isUsed)
        {
            guardsInRadius.Clear();
            isOnCooldown = true;
            suspiciousnessMeter.AddToMeter(meterFillAmount);
            if (Input.GetMouseButton(0)) cooldownText.SetActive(true);
            Cursor.visible = true;;
            cooldownTimer = abilityCooldown;
            cooldownText.SetActive(true);
            isInRange = false;

            if (vfx != null)
                vfx.SetActive(false);
        }

    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    IEnumerator DeactivateDelay()
    {
        yield return new WaitForSeconds(1.5f);
        isDeactivating = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isInRange = false;
        }
    }
}


