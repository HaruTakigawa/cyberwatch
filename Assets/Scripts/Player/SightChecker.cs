﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightChecker : MonoBehaviour
{
    public bool isSeen;
    public SuspiciousnessMeter meter;
    private void Start()
    {
        if (meter == null)
            meter = FindObjectOfType<SuspiciousnessMeter>();
        isSeen = false;
    }

    private void Update()
    {
        // If Guard has you on their list, you are seen
        if (this.GetComponent<FieldOfView>().visibleTarget.Count > 0 || this.GetComponent<GuardsBehaviour>().guardState == GuardState.Chase)
        {
            isSeen = true;
            meter.IncreaseMeter();
            meter.TimerReset();
        }

        // If Guard doesn't have you on the list, you are not seen
        else if (this.GetComponent<FieldOfView>().visibleTarget.Count <= 0 || this.GetComponent<GuardsBehaviour>().guardState != GuardState.Chase)
        {
            isSeen = false;
            if (meter.curMeter <= 0) return;
            meter.DecreaseMeter();
        }
    }
}
