﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerAbilities : MonoBehaviour
{
    public bool hasSelectedBlink;
    public bool hasSelectedPing;
    public bool hasSelectedDeactivateGuard;

    [SerializeField] private List<Abilities>    abilities;
    [SerializeField] private GameObject         endpointGameObject;
    [SerializeField] private GameObject         castRadius;
    [SerializeField] private AbilityIndicator   abilityIndicator;
    // Start is called before the first frame update
    void Start()
    {
        hasSelectedBlink            = false;
        hasSelectedPing             = false;
        hasSelectedDeactivateGuard  = false;
        Cursor.visible = true;
        castRadius.SetActive(true);    
    }

    // Update is called once per frame
    void Update()
    {
        Abilities();
        DeselectAbilities();
    }

    public void Abilities()
    {
        // Checks for the current active scene
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        if (abilities != null)
        {
            if (abilities[0].isUsed == false && abilities[0].isOnCooldown == false)
            {
                // Blink
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    Debug.Log("hasSelectedBlink");
                    hasSelectedBlink = true;
                    hasSelectedPing = false;
                    hasSelectedDeactivateGuard = false;

                    abilityIndicator.blinkIndicator.SetActive(true);
                    abilityIndicator.pingIndicator.SetActive(false);
                    abilityIndicator.deactivateIndicator.SetActive(false);

                    castRadius.SetActive(false);
                    Cursor.visible = false;
                }
            }

            // Can only be used in Stages 2 and 3
            if (abilities[1].isUsed == false  && abilities[1].isOnCooldown == false && sceneName == "Main 2" || sceneName == "Main 3")
            {
                // Ping
                if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    Debug.Log("hasSelectedPing");
                    hasSelectedPing = true;
                    hasSelectedBlink = false;
                    hasSelectedDeactivateGuard = false;

                    abilityIndicator.pingIndicator.SetActive(true);
                    abilityIndicator.blinkIndicator.SetActive(false);
                    abilityIndicator.deactivateIndicator.SetActive(false);

                    castRadius.SetActive(true);
                    Cursor.visible = false;
                }
            }

            // Can only be used in Stage 3
            if (abilities[2].isUsed == false && abilities[2].isOnCooldown == false && sceneName == "Main 3")
            {
                // Deactivate Guard
                if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    Debug.Log("hasSelectedDeactivateGuard");
                    hasSelectedDeactivateGuard = true;
                    hasSelectedBlink = false;
                    hasSelectedPing = false;

                    abilityIndicator.deactivateIndicator.SetActive(true);
                    abilityIndicator.pingIndicator.SetActive(false);
                    abilityIndicator.blinkIndicator.SetActive(false);

                    endpointGameObject.SetActive(true);
                    castRadius.SetActive(true);
                    Cursor.visible = false;
                }
            }
        }
    }

    public void DeselectAbilities()
    {
        if (Input.GetMouseButtonDown(1))
        {
            hasSelectedBlink = false;
            hasSelectedPing = false;
            hasSelectedDeactivateGuard = false;
            castRadius.SetActive(false);

            abilityIndicator.blinkIndicator.SetActive(false);
            abilityIndicator.pingIndicator.SetActive(false);
            abilityIndicator.deactivateIndicator.SetActive(false);

            if (endpointGameObject != null)
                endpointGameObject.SetActive(false);

            Cursor.visible = true;
        }
    }
}
