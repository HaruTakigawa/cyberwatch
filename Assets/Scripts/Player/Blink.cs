﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : Abilities
{
    public bool             isDashing;
    public float            dashSpeed;
    public PlayerAnimation  anim;
    public CastRangeRenderer CastRangeRenderer;
    private void Start()
    {
        cooldownTimer = abilityCooldown;

        dashSpeed       = dashSpeed * 5;
        isDashing       = false;
        isInRange       = false;

        castRangeCollider.radius = castRange;
    }

    private void Update()
    {
        CastRangeRenderer.SetOrigin(transform.position);
        CastRangeRenderer.SetAimDirection(transform.forward);
        if (isUsed)
            Cooldown();

        if (playerAbilities.hasSelectedBlink)
            BlinkToLocation();

        castRangeCollider.radius = castRange;
        castRangeCollider.transform.position = playerTransform.position;
    }

    public void BlinkToLocation()
    {
        endPoint.SetActive(true);

        // Sets endPoint to the mouse position
        Vector3 worldPosition;
        Vector3 playerPosition;
        Vector3  mousePos = Input.mousePosition;

        mousePos.z = Camera.main.nearClipPlane;
        worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
        playerPosition = player.transform.position;
        endPoint.transform.position = worldPosition;

        float angle = AngleBetweenTwoPoints(player.transform.position, worldPosition);
        player.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle - 90));

        if (!isUsed)
        {
            Vector3 directionToTarget = endPoint.transform.position - player.transform.position;
            if (Physics2D.Raycast(transform.position, directionToTarget, castRange, obstacleMask))
            {
                return;
            }

            if (!isInRange)
            {
                return;
            }
                // Blinks the player to the endPoint's position via left click
                if (Input.GetMouseButtonDown(0))
                {
                    //Changes the direction the Player is facing depending on where they dash

                    isDashing = true;

                    // Dashes the Player towards the endPoint
                    player.transform.position = Vector2.MoveTowards(transform.position, endPoint.transform.position, dashSpeed);

                    playerAbilities.hasSelectedBlink = false;
                    isUsed = true;
                    endPoint.SetActive(false);

                    if (isDashing)
                        StartCoroutine(DashDelay());
                }
        }

        if (isUsed)
            if (Input.GetMouseButtonDown(0))
            {
                isOnCooldown = true;
                cooldownTimer = abilityCooldown;
                //sight.isSeen = false;
                cooldownText.SetActive(true);
                Cursor.visible = true;
                isInRange = false;
            }
    }

    IEnumerator DashDelay()
    {
        yield return new WaitForSeconds(0.1f);
        isDashing = false;
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isInRange = false;
        }
    }
}
