﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int upgradeCodesCollected;
    public bool isMoving;

    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private PauseFunction pause;

    private string HORIZONTAL_INPUT = "Horizontal";
    private string VERTICAL_INPUT = "Vertical";
    private Vector3 playerPos;
    private Vector2 movement;

    // Start is called before the first frame update
    void Start()
    {
        speed = 0.06f;
        upgradeCodesCollected = 0;
        isMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = movement;

        if (pause.isPaused == false)
        {
            movement.x = Input.GetAxisRaw(HORIZONTAL_INPUT);
            movement.y = Input.GetAxisRaw(VERTICAL_INPUT);

            CheckIfMoving();
        }
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * speed);
    }

    // Checks if Player is moving
    public void CheckIfMoving()
    {
        if (rb.velocity.x > 0)
            isMoving = true;

        else if (rb.velocity.x < 0)
            isMoving = true;

        else if (rb.velocity.y > 0)
            isMoving = true;

        else if (rb.velocity.y < 0)
            isMoving = true;

        else
            isMoving = false;
    }
}
