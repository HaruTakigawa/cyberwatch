﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastRadius : MonoBehaviour
{
    public bool hasEnemiesInRadius;

    [SerializeField] private Transform endPoint;
    public float radiusSize;
    [SerializeField] private Ping ping;
    [SerializeField] private Deactivate deactivate;
    [SerializeField] private CastRadiusRenderer castRadiusRenderer;

    // Start is called before the first frame update
    void Start()
    {
        hasEnemiesInRadius = false;
        if (castRadiusRenderer == null)
        {
            print("Cast Radius Renderer not assigned");
        }
    }
    void Update()
    {
        castRadiusRenderer.SetOrigin(transform.position);
        castRadiusRenderer.SetAimDirection(transform.forward);
        castRadiusRenderer.castRadius = radiusSize;
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radiusSize);
    }

    public void CheckForEnemies()
    {
        Vector2 vector = new Vector2(endPoint.transform.position.x, endPoint.transform.position.y);
        Collider2D[] hits = Physics2D.OverlapCircleAll(vector, radiusSize);

        foreach (Collider2D hit in hits)
        {
            if (hit != null)
            {
                if (hit.gameObject.tag == "Guard")
                {
                    hasEnemiesInRadius = true;

                    if (!ping.guardsInRadius.Contains(hit.gameObject))
                        ping.guardsInRadius.Add(hit.gameObject);

                    if (!deactivate.guardsInRadius.Contains(hit.gameObject))
                        deactivate.guardsInRadius.Add(hit.gameObject);

                    Debug.Log("Enemy in radius");
                }
            }
        }
    }
}
