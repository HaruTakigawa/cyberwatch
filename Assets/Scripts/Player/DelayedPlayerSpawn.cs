﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedPlayerSpawn : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject delayedAnim; 
    // Start is called before the first frame update
    void Start()
    {
        player.SetActive(false);
        StartCoroutine(DelayedSpawn());
    }


    IEnumerator DelayedSpawn()
    {
        yield return new WaitForSeconds(1f);
        player.SetActive(true);
        delayedAnim.SetActive(false);
    }
}
