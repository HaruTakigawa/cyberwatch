﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Abilities : MonoBehaviour
{
    public  GameObject          player;
    public  GameObject          endPoint;
    public  GameObject          cooldownText;
    public  GameObject          vfx;
    public  PlayerAbilities     playerAbilities;
    public  SuspiciousnessMeter suspiciousnessMeter;
    public  LayerMask           obstacleMask;
    public  CircleCollider2D    castRangeCollider;
    public  Image               cooldownSprite;
    public  float               abilityCooldown;
    public  float               cooldownTimer;
    public  float               castRange;
    public  float               meterFillAmount;
    public  bool                isUsed;
    public  bool                isInRange;
    public  bool                isOnCooldown;
    public  float                duration;

    public Transform            playerTransform;

    // Start is called before the first frame update
    void Start()
    {
        isUsed = false;
        cooldownSprite.fillAmount = 0;
        cooldownTimer = abilityCooldown;

        if (vfx != null)
            vfx.SetActive(false);

        if (cooldownText != null)
            cooldownText.SetActive(false);
    }

    public void Update()
    {
        vfx.transform.position = endPoint.transform.position;
    }

    public void Cooldown()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer < 0)
        {
            cooldownTimer = 0;
            cooldownSprite.fillAmount = 0;
            isUsed = false;
            cooldownText.SetActive(false);
            isOnCooldown = false;
        }
        
        else
        {
            cooldownSprite.fillAmount = cooldownTimer / abilityCooldown;
        }
    }
}
