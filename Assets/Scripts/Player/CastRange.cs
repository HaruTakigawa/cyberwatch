﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastRange : MonoBehaviour
{
    [SerializeField] private PlayerAbilities    playerAbilities;
    [SerializeField] private Ping               ping;
    [SerializeField] private Blink              blink;
    [SerializeField] private Deactivate         deactivate;

    [SerializeField] private Transform          playerTransform;
    [SerializeField] private GameObject         blinkRange;
    [SerializeField] private GameObject         pingRange;
    [SerializeField] private GameObject         deactivateRange;

    public void OnDrawGizmos()
    {
        // Only shows CastRange visual if Player selects an ability
        if (playerAbilities.hasSelectedBlink || playerAbilities.hasSelectedPing || playerAbilities.hasSelectedDeactivateGuard)
            Gizmos.color = Color.red;

        // Changes CastRange visual size into Ping's
        if (ping.playerAbilities.hasSelectedPing)
        {
            Gizmos.DrawWireSphere(transform.position, ping.castRange);
        }

        // Changes CastRange visual size into Blink's
        if (blink.playerAbilities.hasSelectedBlink)
        {
            Gizmos.DrawWireSphere(transform.position, blink.castRange);
        }

        // Changes CastRange visual size into Deactivate's
        if (deactivate.playerAbilities.hasSelectedDeactivateGuard)
        {
            Gizmos.DrawWireSphere(transform.position, deactivate.castRange);
        }
    }   

    public void Update()
    {
        if (blinkRange != null && pingRange != null && deactivateRange != null)
        {

            if (blink.playerAbilities.hasSelectedBlink && blink.isUsed == false)
            {
                blinkRange.SetActive(true);
                pingRange.SetActive(false);
                deactivateRange.SetActive(false);
            }

            else if (ping.playerAbilities.hasSelectedPing && ping.isUsed == false)
            {
                pingRange.SetActive(true);
                blinkRange.SetActive(false);
                deactivateRange.SetActive(false);
            }

            else if (deactivate.playerAbilities.hasSelectedDeactivateGuard && deactivate.isUsed == false)
            {
                deactivateRange.SetActive(true);
                blinkRange.SetActive(false);
                pingRange.SetActive(false);
            }

            else
            {
                blinkRange.SetActive(false);
                pingRange.SetActive(false);
                deactivateRange.SetActive(false);
            }
        }
    }
}
