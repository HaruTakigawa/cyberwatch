﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UploadVirus : MonoBehaviour
{
    [SerializeField] private bool           foundComputer;
    [SerializeField] private SpriteRenderer player;
    [SerializeField] private GameObject     winScreen;
    [SerializeField] private GameObject     buttonPrompt;
    [SerializeField] private GameObject     exitAnim;
    [SerializeField] private Transform      playerPos;
    [SerializeField] private Transform      computer;
    [SerializeField] private Vector3        offSet;
    [SerializeField] private CameraFollows  cam;
    // Start is called before the first frame update
    void Start()
    {
        exitAnim.SetActive(false);
        buttonPrompt.SetActive(false);
        foundComputer = false;

        if (winScreen != null)
            winScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        UploadToComputer();
    }

    public void UploadToComputer()
    {
        if (foundComputer)
        {
            buttonPrompt.SetActive(true);
            // Input that triggers the Player to complete the level
            if (Input.GetKeyDown(KeyCode.E))
            {
                cam.enabled = false;
                this.GetComponent<Collider2D>().enabled = false;
                this.GetComponent<PlayerController>().enabled = false;
                player.enabled = false;
                //playerPos.position = computer.position;
                exitAnim.transform.position = computer.position + offSet;
                exitAnim.SetActive(true);
                StartCoroutine(DelayedExit());
                Cursor.visible = true;
            }
        }

        else
            buttonPrompt.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Computer")
        {
            foundComputer = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Computer")
        {
            foundComputer = false;
        }
    }

    IEnumerator DelayedExit()
    {
        yield return new WaitForSeconds(1.5f);

        if (winScreen != null)
            winScreen.SetActive(true);
    }
}
