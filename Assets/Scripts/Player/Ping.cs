﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ping : Abilities
{
    [SerializeField] private CastRadius castRadius;

    public float pingRadiusSize;

    public List<GameObject> guardsInRadius = new List<GameObject>();

    public bool isPinging;

    public CastRangeRenderer CastRangeRenderer;

    public void Start()
    {
        isPinging = false;
        cooldownTimer = abilityCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        CastRangeRenderer.SetOrigin(transform.position);
        CastRangeRenderer.SetAimDirection(transform.forward);

        if (isUsed)
            Cooldown();

        if (playerAbilities.hasSelectedPing && !isOnCooldown)
        {
            PingEnemies();
            castRadius.radiusSize = pingRadiusSize;
        }

        //if (!playerAbilities.hasSelectedPing)
        //    isInRange = false;

        castRangeCollider.radius = castRange;
        castRangeCollider.transform.position = playerTransform.position;
    }

    void PingEnemies()
    {
        endPoint.SetActive(true);

        // Sets endPoint to the mouse position
        Vector3 worldPosition;
        Vector3 mousePos = Input.mousePosition;

        mousePos.z = 1;
        worldPosition = Camera.main.ScreenToWorldPoint(mousePos);

        endPoint.transform.position = worldPosition;

        float angle = AngleBetweenTwoPoints(player.transform.position, worldPosition);
        player.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle - 90));

        if (isUsed)
        {
            return;
        }
            if (!isInRange && !isOnCooldown)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                isPinging = true;
                castRadius.CheckForEnemies();

                if (vfx != null)
                    vfx.SetActive(true);

                foreach (GameObject guard in guardsInRadius)
                {
                    if (guard.GetComponent<GuardsBehaviour>().guardType != GuardType.Focused)
                    {
                        if (guard.GetComponent<GuardsBehaviour>().guardState == GuardState.Chase || guard.GetComponent<GuardsBehaviour>().guardState == GuardState.Deactivated)
                        {
                            return;
                        }
                        else
                        {
                            guard.GetComponent<PingedState>().timeLeft = duration;
                            // Changes Guard Behavior to Pinged in guards inside the list
                            guard.GetComponent<GuardsBehaviour>().guardState = GuardState.Pinged;

                            //Sets the Guard's ping location to where the Player clicked
                            guard.GetComponent<PingedState>().pingLocation.position = endPoint.transform.position;
                        }
                    }
                }

                playerAbilities.hasSelectedPing = false;
                isUsed = true;
                endPoint.SetActive(false);

                if (isPinging)
                    StartCoroutine(PingDelay());
            }

        if (isUsed)
            if (Input.GetMouseButtonDown(0))
            {
                isOnCooldown = true;
                cooldownTimer = abilityCooldown;
                suspiciousnessMeter.AddToMeter(meterFillAmount);
                cooldownText.SetActive(true);
                Cursor.visible = true;
                isInRange = false;
                guardsInRadius.Clear();

                if (vfx != null)
                {
                    instantiateParticles();
                    vfx.SetActive(true);
                }
            }
    }

    private void instantiateParticles()
    {
        Vector3 objectPos = endPoint.transform.position;
        GameObject newParticles = Instantiate(vfx, objectPos, Quaternion.identity);
        Destroy(newParticles, 2f);
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    IEnumerator PingDelay()
    {
        yield return new WaitForSeconds(0.7f);
        isPinging = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isInRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isInRange = false;
        }
    }
}
