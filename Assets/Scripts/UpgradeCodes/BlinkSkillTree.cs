﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkSkillTree : SkillTree
{
    [System.Serializable] // Makes struct contents accessable in inspector
    public struct UpgradeDataRange
    {
        public int      cost;
        public float    range;
    };

    [System.Serializable]
    public struct UpgradeDataCooldown
    {
        public int cooldown;
        public int cost;
    }

    [SerializeField] private UpgradeDataRange Tier2BlinkRange;
    [SerializeField] private UpgradeDataRange Tier3BlinkRange;

    [SerializeField] private UpgradeDataCooldown Tier2BlinkCooldown;
    [SerializeField] private UpgradeDataCooldown Tier3BlinkCooldown;

    private int rangeTier;
    private int coolDownTier;

    public void Start()
    {
        rangeTier       = 1;
        coolDownTier    = 1;
    }

    public void UpgradeBlinkRange()
    {
        if (playerController != null && abilities != null)
        {
            if (rangeTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2BlinkRange.cost)
                {
                    abilities.castRange = Tier2BlinkRange.range;
                    playerController.upgradeCodesCollected -= Tier2BlinkRange.cost;
                    rangeTier = 2;
                }
            }

            if (rangeTier == 2)
            {

                if (playerController.upgradeCodesCollected >= Tier3BlinkRange.cost)
                {
                    playerController.upgradeCodesCollected -= Tier3BlinkRange.cost;
                    rangeTier = 3;
                }
            }
        }
    }

    public void UpgradeBlinkCooldown()
    {
        if (playerController != null && abilities != null)
        {
            if (rangeTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2BlinkCooldown.cost)
                {
                    abilities.abilityCooldown = Tier2BlinkCooldown.cooldown;
                    playerController.upgradeCodesCollected -= Tier2BlinkCooldown.cost;
                    coolDownTier = 2;
                }
            }

            if (coolDownTier == 2)
            {
                if (playerController.upgradeCodesCollected >= Tier3BlinkCooldown.cost)
                {
                    abilities.abilityCooldown = Tier3BlinkCooldown.cooldown;
                    playerController.upgradeCodesCollected -= Tier3BlinkCooldown.cost;
                    coolDownTier = 3;
                }
            }
        }
    }
}
