﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingSkillTree : SkillTree
{
    [SerializeField] private Ping ping;

    [System.Serializable] // Makes struct contents accessable in inspector
    public struct UpgradeDataDuration
    {
        public int cost;
        public float duration;
    };

    [System.Serializable]
    public struct UpgradeDataCooldown
    {
        public int cooldown;
        public int cost;
    }

    [System.Serializable]
    public struct UpgradeDataRadius
    {
        public float radius;
        public int   cost;
    }

    [SerializeField] private UpgradeDataDuration Tier2PingDuration;
    [SerializeField] private UpgradeDataDuration Tier3PingDuration;

    [SerializeField] private UpgradeDataCooldown Tier2PingCooldown;
    [SerializeField] private UpgradeDataCooldown Tier3PingCooldown;

    [SerializeField] private UpgradeDataRadius Tier2PingRadius;
    [SerializeField] private UpgradeDataRadius Tier3PingRadius;

    private int durationTier;
    private int radiusTier;
    private int coolDownTier;

    public void Start()
    {
        durationTier    = 1;
        radiusTier      = 1;
        coolDownTier    = 1;
    }

    public void UpgradePingDuration()
    {
        if (playerController != null && abilities != null)
        {
            if (durationTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2PingDuration.cost)
                {
                    abilities.duration = Tier2PingDuration.duration;
                    playerController.upgradeCodesCollected -= Tier2PingDuration.cost;
                    durationTier = 2;
                }
            }

            if (durationTier == 2)
            {

                if (playerController.upgradeCodesCollected >= Tier3PingDuration.cost)
                {
                    abilities.duration = Tier3PingDuration.duration;
                    playerController.upgradeCodesCollected -= Tier3PingDuration.cost;
                    durationTier = 3;
                }
            }
        }
    }

    public void UpgradePingCooldown()
    {
        if (playerController != null && abilities != null)
        {
            if (coolDownTier== 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2PingCooldown.cost)
                {
                    abilities.abilityCooldown = Tier2PingCooldown.cooldown;
                    playerController.upgradeCodesCollected -= Tier2PingCooldown.cost;
                    coolDownTier = 2;
                }
            }

            if (coolDownTier == 2)
            {
                if (playerController.upgradeCodesCollected >= Tier3PingCooldown.cost)
                {
                    abilities.abilityCooldown = Tier3PingCooldown.cooldown;
                    playerController.upgradeCodesCollected -= Tier3PingCooldown.cost;
                    coolDownTier = 3;
                }
            }
        }
    }

    public void UpgradePingRadius()
    {
        if (playerController != null && abilities != null)
        {
            if (radiusTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2PingRadius.cost)
                {
                    ping.pingRadiusSize = Tier2PingRadius.radius;
                    playerController.upgradeCodesCollected -= Tier2PingRadius.cost;
                    radiusTier = 2;
                }
            }

            if (radiusTier == 2)
            {
                if (playerController.upgradeCodesCollected >= Tier3PingRadius.cost)
                {
                    ping.pingRadiusSize = Tier3PingRadius.radius;
                    playerController.upgradeCodesCollected -= Tier3PingRadius.cost;
                    radiusTier = 3;
                }
            }
        }
    }
}
