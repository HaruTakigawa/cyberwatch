﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateSkillTree : SkillTree
{
    [SerializeField] private Deactivate deactivate;

    [System.Serializable] // Makes struct contents accessable in inspector
    public struct UpgradeDataDuration
    {
        public int   cost;
        public float duration;
    };

    [System.Serializable]
    public struct UpgradeDataCooldown
    {
        public int cooldown;
        public int cost;
    }

    [System.Serializable]
    public struct UpgradeDataRadius
    {
        public float radius;
        public int cost;
    }

    [SerializeField] private UpgradeDataDuration Tier2DeactivateDuration;
    [SerializeField] private UpgradeDataDuration Tier3DeactivateDuration;

    [SerializeField] private UpgradeDataCooldown Tier2DeactivateCooldown;
    [SerializeField] private UpgradeDataCooldown Tier3DeactivateCooldown;

    [SerializeField] private UpgradeDataRadius Tier2DeactivateRadius;
    [SerializeField] private UpgradeDataRadius Tier3DeactivateRadius;

    private int durationTier;
    private int radiusTier;
    private int coolDownTier;

    // Start is called before the first frame update
    void Start()
    {
        durationTier    = 1;
        radiusTier      = 1;
        coolDownTier    = 1;
    }

    public void UpgradeDeactivateDuration()
    {
        if (playerController != null && abilities != null)
        {
            if (durationTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2DeactivateDuration.cost)
                {
                    abilities.duration = Tier2DeactivateDuration.duration;
                    playerController.upgradeCodesCollected -= Tier2DeactivateDuration.cost;
                    durationTier = 2;
                }
            }

            if (durationTier == 2)
            {

                if (playerController.upgradeCodesCollected >= Tier3DeactivateDuration.cost)
                {
                    abilities.duration = Tier3DeactivateDuration.duration;
                    playerController.upgradeCodesCollected -= Tier3DeactivateDuration.cost;
                    durationTier = 3;
                }
            }
        }
    }

    public void UpgradeDeactivateCooldown()
    {
        if (playerController != null && abilities != null)
        {
            if (coolDownTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2DeactivateCooldown.cost)
                {
                    abilities.abilityCooldown = Tier2DeactivateCooldown.cooldown;
                    playerController.upgradeCodesCollected -= Tier2DeactivateCooldown.cost;
                    coolDownTier = 2;
                }
            }

            if (coolDownTier == 2)
            {
                if (playerController.upgradeCodesCollected >= Tier3DeactivateCooldown.cost)
                {
                    abilities.abilityCooldown = Tier3DeactivateCooldown.cooldown;
                    playerController.upgradeCodesCollected -= Tier3DeactivateCooldown.cost;
                    coolDownTier = 3;
                }
            }
        }
    }

    public void UpgradeDeactivateRadius()
    {
        if (playerController != null && abilities != null)
        {
            if (radiusTier == 1)
            {
                if (playerController.upgradeCodesCollected >= Tier2DeactivateRadius.cost)
                {
                    deactivate.deactivateRadiusSize = Tier2DeactivateRadius.radius;
                    playerController.upgradeCodesCollected -= Tier2DeactivateRadius.cost;
                    radiusTier = 2;
                }
            }

            if (radiusTier == 2)
            {
                if (playerController.upgradeCodesCollected >= Tier3DeactivateRadius.cost)
                {
                    deactivate.deactivateRadiusSize = Tier3DeactivateRadius.radius;
                    playerController.upgradeCodesCollected -= Tier3DeactivateRadius.cost;
                    radiusTier = 3;
                }
            }
        }
    }
}
