﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillTreeButton : MonoBehaviour
{
    [SerializeField] private GameObject skillTreeButton;
    [SerializeField] private GameObject skillTreeMenu;
    // Start is called before the first frame update
    void Start()
    {
        if (skillTreeButton != null)
            skillTreeButton.SetActive(true);

        if (skillTreeMenu != null)
            skillTreeMenu.SetActive(false);
    }

    public void SkillTreeMenu()
    {
        if (skillTreeMenu != null && skillTreeButton != null)
        {
            skillTreeMenu.SetActive(true);
            skillTreeButton.SetActive(false);
        }
    }

    public void SkillTreeExit()
    {
        if (skillTreeMenu != null && skillTreeButton != null)
        {
            skillTreeButton.SetActive(true);
            skillTreeMenu.SetActive(false);
        }
    }
}
