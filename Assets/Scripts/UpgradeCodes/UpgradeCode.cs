﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeCode : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerController.upgradeCodesCollected++;
            Destroy(this.gameObject);
        }
    }
}
