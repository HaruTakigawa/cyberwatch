﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleWallTrigger : MonoBehaviour
{
    [SerializeField] private GameObject invisibleWall;
    [SerializeField] private bool isOnTrigger;
    // Start is called before the first frame update
    void Start()
    {
        invisibleWall.SetActive(false);
    }

    private void Update()
    {
        if (isOnTrigger)
            invisibleWall.SetActive(true);

        else
            invisibleWall.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isOnTrigger = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Endpoint")
        {
            isOnTrigger = false;
        }
    }
}
