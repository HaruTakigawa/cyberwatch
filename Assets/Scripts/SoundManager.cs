﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;


public class SoundManager : MonoBehaviour
{
    public Sound[] sounds;
    public static SoundManager instance;
    SuspiciousnessMeter sus;
    AudioListener listener;
    public TextMeshProUGUI textDisplay;
    private bool isMuted;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        //DontDestroyOnLoad(transform.gameObject);
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.Volume;
            s.source.pitch = s.Pitch;
            s.source.loop = s.Loop;
        }
    }
    void Start()
    {
        sus = GameObject.Find("Canvas").GetComponentInChildren<SuspiciousnessMeter>();
        //listener = GameObject.Find("MainCamera").GetComponent<AudioListener>();
        isMuted = false;
        Play("Theme");
    }

    // Update is called once per frame
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound not found: " + s.name);
        }
        s.source.Play();
    }
    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound not found: " + s.name);
        }
        s.source.Stop();
    }
    public bool isPlaying(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound not found: " + s.name);
        }

        return s.source.isPlaying;
    }
    public bool StopLooping(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound not found: " + s.name);
        }

        return s.source.loop = false;
    }
    public bool StartLooping(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound not found: " + s.name);
        }

        return s.source.loop = true;
    }

    public void adjustVolume(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        s.source.volume = 0.1f;
    }

    IEnumerator FadeAudiosource(string name, float duration, float targetVolume)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        float currentTime = 0;
        float start = s.source.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            s.source.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        yield break;
    }

    IEnumerator DelayedStop(string name, float duration)
    {
        yield return new WaitForSeconds(.7f);
        Stop(name);
    }

    public void FadeOutTheme()
    {
        StartCoroutine(FadeAudiosource("Theme", 1f, 0f));
    }

    public void FadeSound(string name)
    {
        StartCoroutine(FadeAudiosource(name, 1f, 0f));
    }

    public void ToggleAudio()
    {
        if(isMuted)
        {
            Debug.Log("Audio unmuted");
            textDisplay.text = "AUDIO UNMUTED";
            AudioListener.volume = 1f;
            isMuted = false;
        }
        else if(!isMuted) 
        {
            Debug.Log("Audio Muted");
            textDisplay.text = "AUDIO MUTED";
            AudioListener.volume = 0f;
            isMuted = true;
        }
    }

    void FixedUpdate()
    {
        if (sus.curMeter >= 100)
        {
            if (!isPlaying("Alert"))
            {
                Debug.Log(isPlaying("Alert"));
                Debug.Log(sus.curMeter);
                StartCoroutine(FadeAudiosource("Theme", 2f, 0f));
                StartCoroutine(FadeAudiosource("Alert", .1f, 1f));
                Play("Alert");
            }
        }
        else
        {
            if(isPlaying("Alert"))
            {
                Debug.Log(isPlaying("Alert"));
                Debug.Log(sus.curMeter);
                StartCoroutine(FadeAudiosource("Alert", 2f, 0f));
                StartCoroutine(FadeAudiosource("Theme", 2f, 1f));
                Stop("Alert");
            }
        }

    }

}

