﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirewallTrigger : MonoBehaviour
{
    public bool isActive;

    [SerializeField] private GameObject fireWall;
    [SerializeField] private GameObject semiActiveFireWall;
    [SerializeField] private GameObject inactiveFireWall;
    [SerializeField] private bool       hasActivatedOnce;

    // Start is called before the first frame update
    void Start()
    {
        fireWall.SetActive(false);
        semiActiveFireWall.SetActive(false);
        inactiveFireWall.SetActive(true);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isActive = true;
            hasActivatedOnce = true;

            if (inactiveFireWall != null && semiActiveFireWall != null)
            {
                inactiveFireWall.SetActive(false);
                semiActiveFireWall.SetActive(false);
            }

            if (fireWall != null)
                fireWall.SetActive(true);
            
        }
    }

    // Closes Semi Active Firewall if Player leaves collision area
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && hasActivatedOnce)
        {
            fireWall.SetActive(false);
            semiActiveFireWall.SetActive(true);
            inactiveFireWall.SetActive(false);
        }
    }
}
