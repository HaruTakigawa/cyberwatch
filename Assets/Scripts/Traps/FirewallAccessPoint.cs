﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirewallAccessPoint : MonoBehaviour
{
    [SerializeField] private bool isOnTerminal;
    [SerializeField] private GameObject buttonPrompt;
    [SerializeField] private GameObject deactivatedPoint;
    public GameObject fireWall;

    private bool isDeactivated;

    public void Start()
    {
        fireWall.SetActive(true);
        buttonPrompt.SetActive(false);
        isDeactivated = false;
        deactivatedPoint.SetActive(false);
    }

    public void Update()
    {
        if (isOnTerminal)
        {
            if (!isDeactivated)
            {
                buttonPrompt.SetActive(true);
                deactivatedPoint.SetActive(false);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                fireWall.SetActive(false);
                isDeactivated = true;
                buttonPrompt.SetActive(false);
            }

            if (isDeactivated)
            {
                deactivatedPoint.SetActive(true);
            }
        }

        else
            buttonPrompt.SetActive(false);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOnTerminal = true;
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOnTerminal = false;
        }
    }
}
