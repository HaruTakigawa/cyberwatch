﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiVirusSensor : MonoBehaviour
{
    [SerializeField] private List<GameObject>       guards = new List<GameObject>();
    [SerializeField] private SuspiciousnessMeter    suspiciousnessMeter;
    [SerializeField] private Transform              sensorLocation;
    [SerializeField] private float                  radiusSize;
    [SerializeField] private Transform              radiusLocation;
    [SerializeField] private float                  meterFillAmount = 20;
    public Collider2D[] hits;

    public void Update()
    {
        
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(radiusLocation.position, radiusSize);
    }

    public void CheckForEnemies()
    {
        hits = Physics2D.OverlapCircleAll(radiusLocation.position, radiusSize);

        foreach (Collider2D hit in hits)
        {
            if (hit != null)
            {
                if (hit.gameObject.tag == "Guard")
                {
                    if (!guards.Contains(hit.gameObject))
                        guards.Add(hit.gameObject);
                }
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        // Pings guards in the list when player colliders with sensor
        if (collision.gameObject.tag == "Player")
        {
            CheckForEnemies();
            suspiciousnessMeter.AddToMeter(meterFillAmount);
            // Connect enemy pinged function here
            if (guards != null)
            {
                foreach (GameObject grd in guards)
                {
                    if (grd.GetComponent<GuardsBehaviour>().guardState == GuardState.Chase || grd.GetComponent<GuardsBehaviour>().guardState == GuardState.Deactivated)
                    {
                        return;
                    }
                    else
                    {
                        // Pings the enemies in the list
                        grd.GetComponent<GuardsBehaviour>().guardState = GuardState.Pinged;

                        //Sets the location of the object so that the enemy approaches the area
                        grd.GetComponent<PingedState>().pingLocation.position = sensorLocation.position;
                    }                 
                }
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        // Removes Guard from list if it leaves the area
        if (collision.gameObject.tag == "Player")
        {

        }
    }
}
