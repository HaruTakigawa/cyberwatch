﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGuards : MonoBehaviour
{
    public List<GameObject> GuardsToBeSpawnedList;
    public SuspiciousnessMeter susMeter;
    private bool isSpawned = false;
    private void Start()
    {
        if (susMeter == null)
            susMeter = FindObjectOfType<SuspiciousnessMeter>();
    }
    private void Update()
    {
        if (isSpawned)
            return;
        if(susMeter.curMeter >= susMeter.maxMeter)
        {
            foreach (GameObject guard in GuardsToBeSpawnedList)
            {
                if (guard.activeInHierarchy)
                {
                    continue;
                }
                else
                {
                    guard.SetActive(true);
                }
            }
            isSpawned = true;
        }  
    }
}
