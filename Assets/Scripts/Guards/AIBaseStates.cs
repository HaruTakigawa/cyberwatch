﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBaseStates : MonoBehaviour
{
    public float waitTime;
    public float degreesToRotate;
    public float targetPositionThreshold;

    public float timeLeft;
    protected bool isAtPoint = false;

    public float timeToRotate;
    protected float timeLeftRotation;
    protected GuardsBehaviour guardsBehaviour;
    protected NavMeshAgent agent;
    protected NavMeshPath path;
    public bool isRotationSet;
    public bool isDoneLooking;
    public virtual void Start()
    {
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }
        path = new NavMeshPath();
        if (guardsBehaviour == null)
            guardsBehaviour = this.GetComponent<GuardsBehaviour>();
        timeLeft = waitTime;
        timeLeftRotation = timeToRotate;
    }
    public virtual void LookAround()
    {
        float rotate = degreesToRotate / waitTime;
        agent.transform.Rotate(Vector3.forward * rotate * Time.deltaTime);
    }
    public virtual void RotateToNextPoint(Transform TargetPosition)
    {
        Vector3 dir = TargetPosition.position - transform.forward;
        float dot = Vector2.Dot(TargetPosition.position, transform.position);
        float cos = Mathf.Cos(dot);
        float angle = cos * 180 / Mathf.PI;
        float rotate = cos / timeToRotate;
        agent.transform.Rotate(Vector3.forward * rotate * Time.deltaTime);
    }
    public virtual void TimeInSpot(Transform targetPosition)
    {
        if (Vector2.Distance(transform.position, targetPosition.position) <= targetPositionThreshold)
        {
            if (timeLeft <= 0)
            {
                isAtPoint = false;
                timeLeft = waitTime;
            }
            else
            {
                isAtPoint = true;
                timeLeft -= Time.deltaTime;
            }
        }
        else
        {
            isAtPoint = false;
        }
    }
    public virtual void SetRotandPos()
    {
        Vector3 curRotation = transform.localRotation.eulerAngles;
        curRotation.x = 0;
        curRotation.y = 0;
        transform.localRotation = Quaternion.Euler(curRotation);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }
    Vector3 GetVectorFromAngle(float angle)
    {
        float angleRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
    }
    float GetAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;
        return n;
    }
}
