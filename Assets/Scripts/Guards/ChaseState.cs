﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ChaseState : AIBaseStates
{
    public float newViewRange;
    [HideInInspector] public Transform targetLastPos;


    private FieldOfView fieldOfView;
    [HideInInspector] public Transform target = null;

    public override void Start()
    {
        base.Start();
        if (fieldOfView == null)
        {
            fieldOfView = this.GetComponent<FieldOfView>();
        }
    }

    private void Update()
    {
        // If the guard isn't in chase mode return
        if (guardsBehaviour.guardState != GuardState.Chase) return;
        target = fieldOfView.visibleTarget[0];

        targetLastPos.position = target.position;

        fieldOfView.viewRange = newViewRange;

        if (Vector2.Distance(transform.position, target.position) > targetPositionThreshold)
        {
            transform.up = target.position - transform.position;
            agent.CalculatePath(target.position, path);
            if (path.status == UnityEngine.AI.NavMeshPathStatus.PathPartial)
            {
                guardsBehaviour.SetToDefaultState();
            }
            else
            {
                agent.SetDestination(target.position);
            }          
            SetRotandPos();
        }
    }
}
