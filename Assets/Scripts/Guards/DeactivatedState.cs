﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivatedState : AIBaseStates
{
    [SerializeField] private GameObject fieldOfViewRenderer;
    private FieldOfView fov;
    public override void Start()
    {
        base.Start();
        timeLeft = waitTime;
        if (fieldOfViewRenderer == null)
        {
            print("FoV renderer not assigned");
        }
        if (fov == null)
        {
            fov = this.GetComponent<FieldOfView>();
        }
    }

    private void Update()
    {
        if (guardsBehaviour.guardState != GuardState.Deactivated) return;

        if (timeLeft < 0)
        {
            timeLeft = waitTime;
            guardsBehaviour.SetToDefaultState();
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            fieldOfViewRenderer.SetActive(true);
            fov.enabled = true;
        }
        else
        {
            agent.SetDestination(this.transform.position);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            fieldOfViewRenderer.SetActive(false);
            fov.visibleTarget.Clear();
            fov.enabled = false;
            timeLeft -= Time.deltaTime;
        }
    }
}
