﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GuardState
{
    Idle = 1,
    Patrol = 2,
    Chase = 3,
    Wander = 4,
    Pinged = 5,
    Deactivated = 6
}
public enum GuardType
{
    // Doesn't patrol Smal
    Lazy,
    Focused,
    Normal

}
public class GuardsBehaviour : MonoBehaviour
{
    public GuardState guardState = GuardState.Patrol;
    public GuardType guardType;

    public float timeLeftToChase;

    [HideInInspector] public float timeLeft;

    private FieldOfView fov;
    private PingedState pinged;
    private ChaseState chase;
    private PatrolState patrol;
    private WanderState wander;
    private DeactivatedState deactivated;
    [SerializeField] private FieldOfViewRenderer fovRenderer;
    private float oldViewRange;
    private Vector3 aimDir;
    private SuspiciousnessMeter susMeter;
    private void Start()
    {
        if (fov == null)
            fov = this.GetComponent<FieldOfView>();
        if (pinged == null)
            pinged = this.GetComponent<PingedState>();
        if (chase == null)
            chase = this.GetComponent<ChaseState>();
        if (patrol == null)
            patrol = this.GetComponent<PatrolState>();
        if (wander == null)
            wander = this.GetComponent<WanderState>();
        if (deactivated == null)
            deactivated = this.GetComponent<DeactivatedState>();
        if (fovRenderer == null)
            fovRenderer = this.GetComponentInChildren<FieldOfViewRenderer>();
        if (susMeter == null)
            susMeter = FindObjectOfType<SuspiciousnessMeter>();
        timeLeft = timeLeftToChase;
        guardState = GuardState.Patrol;
        oldViewRange = fov.viewRange;
    }
    private void Update()
    {
        if (guardState == GuardState.Deactivated) return;

        // There was a target that the guard was chasing, but it disappeared. Set guard to wander
        if (fov.visibleTarget.Count == 0 && guardState == GuardState.Chase)
        {
            SetFoVOriginAndDir(fovRenderer.transformForward);
            guardState = GuardState.Wander;
            wander.wanderSpot.position = chase.targetLastPos.position;
        }
        if (fov.visibleTarget.Count == 0)
        {
            SetFoVOriginAndDir(fovRenderer.transformForward);
            timeLeft = timeLeftToChase;
            return;
        }

        // Sets to chase when previous state is wander && there is a player
        if (fov.visibleTarget[0].tag == "Player" && guardState == GuardState.Wander)
        {
            guardState = GuardState.Chase;
            SetFoVOriginAndDir(fovRenderer.transformForward);
            wander.Start();
        }
        // Immediately chase when susMeter is maxed
        else if (susMeter.curMeter >= susMeter.maxMeter && fov.visibleTarget[0].tag == "Player")
        {
            guardState = GuardState.Chase;
            SetFoVOriginAndDir(fovRenderer.transformForward);
            wander.Start();
        }
        else if (fov.visibleTarget[0].tag == "Player")
        {
            // Setting the guard to chase 
            if (guardState != GuardState.Chase)
                SetFoVOriginAndDir(fovRenderer.transformForward);
            // If guard is already at chase return;
            if (guardState == GuardState.Chase)
            {
                SetFoVOriginAndDir(fovRenderer.transformForward);
                return;
            }
            if (timeLeft < 0)
            {
                timeLeft = timeLeftToChase;
                guardState = GuardState.Chase;
                SetFoVOriginAndDir(fovRenderer.transformForward);
                wander.Start();
            }
            else
            {
                SetFoVOriginAndDir(fovRenderer.transformForward);
                timeLeft -= Time.deltaTime;
            }
        }
    }
    public void SetToDefaultState()
    {
        // Put anything to reset here
        guardState = GuardState.Patrol;
        chase.target = null;
        fov.viewRange = oldViewRange;
        patrol.isRotationSet = false;
        wander.isRotationSet = false;
        pinged.isRotationSet = false;
    }
    public void SetFoVOriginAndDir(Transform targetPos)
    {
        aimDir = (targetPos.position - transform.position).normalized;
        fovRenderer.SetOrigin(transform.position);
        fovRenderer.SetAimDirection(aimDir);
    }
    // Add a function that resets the values of the AI state when it is re enabled. 
    // Use a list of AI Base State and compare which one needs to be resetted.
    // The one to be resetted should be the next state of the AI

    // for Wander, Pinged, & Deactivate call SetToDefaultState once they are done.
}
