﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWanderLocation : MonoBehaviour
{
    [SerializeField] private Transform guardPosition;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Wall")
        {
            transform.position = Vector3.MoveTowards(this.transform.position, guardPosition.position, 1f * Time.deltaTime);
        }
        if (collision.tag == "WanderPoint")
        {
            transform.position = Vector3.MoveTowards(this.transform.position, guardPosition.position, 1f * Time.deltaTime);
        }
    }
}
