﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderState : AIBaseStates
{
    [HideInInspector] public Transform wanderSpot;
    public float wanderRadius;
    public float wanderPointsCount;
    public LayerMask obstacleMask;

    private float pointsCount;
    public override void Start()
    {
        base.Start();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        pointsCount = 0;
    }
    private void Update()
    {
        // If guard state is not wander, exit
        if (guardsBehaviour.guardState != GuardState.Wander) return;

        TimeInSpot(wanderSpot);
        SetRotandPos();
        isRotationSet = false;
        if (!isAtPoint)
        {
            // Go to destination
            // if there is no path to destination go back to default
           
            agent.CalculatePath(wanderSpot.position, path);
            if (path.status == UnityEngine.AI.NavMeshPathStatus.PathPartial)
            {
                guardsBehaviour.SetToDefaultState();
            }
            else
            {
                agent.SetDestination(wanderSpot.position);
            }
       
            if (!isRotationSet)
            {
                Vector3 dir = wanderSpot.position - transform.position;
                transform.up = dir;

                isRotationSet = true;
            }
        }
        
        if (isAtPoint)
        {
            isRotationSet = false;
            LookAround();
        }

    }
    void GetNextSpot()
    {
        while (true)
        {
            Vector2 randSpotInRadius = Random.insideUnitCircle * wanderRadius;
            Vector2 randomizedSpot = randSpotInRadius + (Vector2)wanderSpot.position;

            Vector2 dir = (randomizedSpot - (Vector2)transform.position).normalized;
            float dis = dir.magnitude;
            if (!Physics2D.Raycast(transform.position, dir, dis, obstacleMask))
            {
                wanderSpot.position = randomizedSpot;
                break;
            }
        }
    }
    public override void TimeInSpot(Transform targetPosition)
    {
        Vector2 dir = (targetPosition.position - transform.position).normalized;
        float dis = dir.magnitude;

        if (dis <= targetPositionThreshold)
        {
            if (timeLeft <= 0)
            {
                isAtPoint = false;
                timeLeft = waitTime;

                if (pointsCount >= wanderPointsCount)
                {
                    guardsBehaviour.SetToDefaultState();
                }
                else
                {
                    GetNextSpot();
                    pointsCount++;
                }
            }
            else
            {
                isAtPoint = true;
                timeLeft -= Time.deltaTime;
            }
        }
        else
        {
            isAtPoint = false;
        }
    }
    public override void LookAround()
    {
        base.LookAround();
    }
}
