﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingedState : AIBaseStates
{
    [HideInInspector] public Transform pingLocation;
    public override void Start()
    {
        base.Start();
        timeLeft = waitTime;
    }

    private void Update()
    {
        if (guardsBehaviour.guardState != GuardState.Pinged) return;
        TimeInSpot(pingLocation);
        SetRotandPos();
        isRotationSet = false;
        if (!isAtPoint)
        {
            
            agent.CalculatePath(pingLocation.position, path);
            if (path.status == UnityEngine.AI.NavMeshPathStatus.PathPartial)
            {
                guardsBehaviour.SetToDefaultState();
            }
            else
            {
                agent.SetDestination(pingLocation.position);
            }
            if (!isRotationSet)
            {
                Vector3 dir = pingLocation.position - transform.position;
                transform.up = dir;

                isRotationSet = true;
            }
        }
        // If transform is on the ping location
        if (isAtPoint)
        {
            isRotationSet = false;
            LookAround();
        }
    }
    public override void TimeInSpot(Transform targetPosition)
    {
        Vector2 dir = (targetPosition.position - transform.position).normalized;
        float dis = dir.magnitude;

        if (dis <= targetPositionThreshold)
        {
            if (timeLeft <= 0)
            {
                isAtPoint = false;
                timeLeft = waitTime;
                guardsBehaviour.SetToDefaultState();
            }
            else
            {
                isAtPoint = true;
                timeLeft -= Time.deltaTime;
            }
        }
        else
        {
            isAtPoint = false;
        }
    }

    public override void LookAround()
    {
        base.LookAround();
    }
}
