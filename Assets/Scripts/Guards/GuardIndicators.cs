﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardIndicators : MonoBehaviour
{
    public GuardsBehaviour  guard;
    public Transform        guardPosition;
    public GameObject       pinged;
    public GameObject       detected;
    public GameObject       deactivated;
    public GameObject       wander;

    // Start is called before the first frame update
    void Start()
    {
        pinged.SetActive(false);
        detected.SetActive(false);
        deactivated.SetActive(false);
        wander.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // Deactivated
        if (guard.guardState == GuardState.Deactivated)
        {
            pinged.SetActive(false);
            detected.SetActive(false);
            deactivated.SetActive(true);
            wander.SetActive(false);
        }

        // Wander
        if (guard.guardState == GuardState.Wander)
        {
            pinged.SetActive(false);
            detected.SetActive(false);
            deactivated.SetActive(false);
            wander.SetActive(true);
        }

        // Chase
        if (guard.timeLeft <= 0 || guard.guardState == GuardState.Chase)
        {
            pinged.SetActive(false);
            detected.SetActive(true);
            deactivated.SetActive(false);
            wander.SetActive(false);
        }

        // Pinged
        if (guard.guardState == GuardState.Pinged)
        {
            pinged.SetActive(true);
            detected.SetActive(false);
            deactivated.SetActive(false);
            wander.SetActive(false);
        }

        // Patrol
        if (guard.guardState == GuardState.Patrol)
        {
            pinged.SetActive(false);
            detected.SetActive(false);
            deactivated.SetActive(false);
            wander.SetActive(false);
        }
    }
}
