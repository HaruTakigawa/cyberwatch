﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : AIBaseStates
{
    [SerializeField] public List<Transform> patrolSpots;

    private int patrolSpotsIndex;
    public override void Start()
    {
        base.Start();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        // Resets Values
        patrolSpotsIndex = 0;
        timeLeft = waitTime;
        isAtPoint = false;
        isRotationSet = false;
        if (guardsBehaviour.guardState != GuardState.Patrol)
            guardsBehaviour.guardState = GuardState.Patrol;
    }
    private void Update()
    {
        if (guardsBehaviour.guardState != GuardState.Patrol) return;
        //if (guardsBehaviour.guardType == GuardType.Lazy) return;

        TimeInSpot(patrolSpots[patrolSpotsIndex]);
        SetRotandPos();
        isRotationSet = false;
        //Agent is travelling
        if (!isAtPoint)
        {
            agent.SetDestination(patrolSpots[patrolSpotsIndex].position);
            if (!isRotationSet)
            {
                Vector3 dir = patrolSpots[patrolSpotsIndex].position - transform.position;
                transform.up = dir;
                isRotationSet = true;
            }
        }
        //Agent is looking around
        if (isAtPoint)
        {
            isRotationSet = false;
            LookAround();
        }
    }
    public override void TimeInSpot(Transform targetPosition)
    {
        Vector2 dir = targetPosition.position - transform.position;
        float dis = dir.magnitude;
        if (dis <= targetPositionThreshold)
        {
            if (timeLeft < 0)
            {
                if (timeLeftRotation > 0)
                {
                    timeLeftRotation -= Time.deltaTime;
                    if (patrolSpotsIndex >= patrolSpots.Count - 1)
                    {
                        RotateToNextPoint(patrolSpots[0]);
                    }
                    else
                        RotateToNextPoint(patrolSpots[patrolSpotsIndex + 1]);
                }
                else
                {
                    isAtPoint = false;
                    timeLeft = waitTime;
                    timeLeftRotation = timeToRotate;
                    if (patrolSpotsIndex >= patrolSpots.Count - 1)
                    {
                        patrolSpotsIndex = 0;
                    }
                    else patrolSpotsIndex++;
                }

            }
            else
            {
                isAtPoint = true;
                timeLeft -= Time.deltaTime;

            }
        }
        else
        {
            isAtPoint = false;
        }
    }
    public override void LookAround()
    {
        base.LookAround();
    }
}
