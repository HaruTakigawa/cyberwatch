﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(FieldOfView))]
public class FieldOfViewEditor : Editor
{
    private void OnSceneGUI()
    {
        FieldOfView fow = (FieldOfView)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fow.transform.position, Vector3.back, Vector3.left, 360, fow.viewRange);
        Vector3 viewAngleA = fow.DirFromAngle(-fow.viewAngle / 2, false);
        Vector3 viewAngleB = fow.DirFromAngle(fow.viewAngle / 2, false);
        Handles.color = Color.red;
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleA * fow.viewRange);
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleB * fow.viewRange);
       
        foreach (Transform visibleTarget in fow.visibleTarget)
        {
            Handles.DrawLine(fow.transform.position, visibleTarget.position);
        }
    }
}
