﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerHUD : MonoBehaviour
{
    [SerializeField] private PlayerController   playerController;
    [SerializeField] private TextMeshProUGUI    upgradeCodesText;         
    

    // Update is called once per frame
    void Update()
    {
        if (upgradeCodesText != null && playerController != null)
            upgradeCodesText.text = "Upgrade Codes: " + playerController.upgradeCodesCollected.ToString();   
    }
}
