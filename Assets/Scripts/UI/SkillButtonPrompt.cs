﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillButtonPrompt : MonoBehaviour
{
    [SerializeField] private GameObject prompt1;
    [SerializeField] private GameObject prompt2;
    [SerializeField] private GameObject prompt3;

    [SerializeField] private bool forPrompt1;
    [SerializeField] private bool forPrompt2;
    [SerializeField] private bool forPrompt3;

    // Start is called before the first frame update
    void Start()
    {
        prompt1.SetActive(false);
        prompt2.SetActive(false);
        prompt3.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (forPrompt1)
                prompt1.SetActive(true);

            if (forPrompt2)
                prompt2.SetActive(true);

            if (forPrompt3)
                prompt3.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (forPrompt1)
            prompt1.SetActive(false);

        if (forPrompt2)
            prompt2.SetActive(false);

        if (forPrompt3)
            prompt3.SetActive(false);
    }
}
