﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollows : MonoBehaviour
{
    [SerializeField] private float      speed;
    [SerializeField] private Transform  target;
    [SerializeField] private Vector3    offSet;

    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        //transform.position = target.position;
    }

    void LateUpdate()
    {
        if (target != null)
        {
            // The offSet sets the position of the camera's distance from the player while following
            Vector3 desiredPosition = target.position + offSet;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, speed);
            transform.position = smoothedPosition;
        }
    }
}
