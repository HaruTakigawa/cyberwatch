﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityIndicator : MonoBehaviour
{
    [SerializeField] private PlayerAbilities    playerAbilities;
    [SerializeField] private Blink              blink;
    [SerializeField] private Ping               ping;
    [SerializeField] private Deactivate         deactivate;

    public GameObject blinkIndicator;
    public GameObject pingIndicator;
    public GameObject deactivateIndicator;

    //Start is called before the first frame update
    void Start()
    {
        blinkIndicator.SetActive(false);
        pingIndicator.SetActive(false);
        deactivateIndicator.SetActive(false);
    }

    //Update is called once per frame
    void Update()
    {
        if (blink.isOnCooldown)
            blinkIndicator.SetActive(false);

        if (ping.isOnCooldown)
            pingIndicator.SetActive(false);

        if (deactivate.isOnCooldown)
            deactivateIndicator.SetActive(false);
    }
}
