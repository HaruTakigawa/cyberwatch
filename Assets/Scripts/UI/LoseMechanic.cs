﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseMechanic : MonoBehaviour
{
    private string previousScene;

    public void Start()
    {
        // Calls the scene name stored onCollision
        previousScene = PlayerPrefs.GetString("scene");    
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        if (collision.gameObject.tag == "Guard")
        {
            Time.timeScale = 0;
            Cursor.visible = true;

            // Store's the scene's name to be used as the scene where the retry button will take you.
            PlayerPrefs.SetString("scene", sceneName);
            SceneManager.LoadScene("LoseScene");
        }
    }

    public void RetryButton()
    {
        SceneManager.LoadScene(previousScene);

        Time.timeScale = 1;
    }
}
