﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class DialogueManager : MonoBehaviour
{

    public TextMeshProUGUI textDisplay;
    public GameObject[] image;
    public string[] sentences;
    private int index;

    public float typeSpeed;

    public GameObject contButton;
    public GameObject controls;
    public GameObject play;

   // public Animator anim;
    public GameObject cam;

    private void Start()
    {
        Time.timeScale = 1f;
        index = 0;
        ResetTextDisplay();
        StartCoroutine(Type());
        contButton.SetActive(false);
        play.SetActive(false);

        //cam = GameObject.FindGameObjectWithTag("MainCamera");
        //anim = cam.GetComponent<Animator>();
        //anim.speed = 0;
    }

    private void Update()
    {
        if (textDisplay.text == sentences[index] && !(index >= sentences.Length - 1))
        {
            contButton.SetActive(true);
        }
        if (Input.anyKey)
        {
            if (contButton.activeSelf)
            {
                NextSentence();
                ShowImage(index);

            }
        }

    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typeSpeed);
        }
        if (index >= sentences.Length - 1)
        {
            ShowControls();
        }
    }

    public void NextSentence()
    {
        contButton.SetActive(false);

        if (index < sentences.Length - 1)
        {
            index++;
            ResetTextDisplay();

            StartCoroutine(Type());
        }
        else
        {
            ResetTextDisplay();
            contButton.SetActive(false);
        }
    }

    public void ShowImage(int i)
    {
        if (image[i] != null)
        {
            HidePreviousImage(i);
            image[i].gameObject.SetActive(true);
        }

    }

    void HidePreviousImage(int i)
    {
        if (image[i - 1] != null)
        {
            image[i - 1].gameObject.SetActive(false);
        }
    }

    private void ResetTextDisplay()
    {
        textDisplay.text = "";
    }

    private void ShowControls()
    {
        //anim.speed = 1;
        Cursor.visible = true;
        contButton.SetActive(false);
        controls.SetActive(true);
        play.SetActive(true);
    }
}
