﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuspiciousnessMeter : MonoBehaviour
{
    [SerializeField] private List<SightChecker> sightChecker;
    [SerializeField] private GameObject virusAlert;
    [SerializeField] private Animator anim;
    public Image fillBar;

    public float waitTimeLeft;

    public float rateOfChange;
    public float maxMeter = 100;
    public float curMeter = 0;

    public float waitTimer;
    // Start is called before the first frame update
    void Start()
    {
        if (virusAlert != null)
            virusAlert.SetActive(false);

        anim.SetBool("IsAtForm4", false);
        anim.SetBool("IsAtForm3", false);
        anim.SetBool("IsAtForm2", false);
        anim.SetBool("IsAtForm5", false);
        waitTimer = waitTimeLeft;
    }

    // Update is called once per frame
    void Update()
    {
        SwitchForAnim();
        if (curMeter >= maxMeter)
        {
            curMeter = maxMeter;

            if (virusAlert != null)
                virusAlert.SetActive(true);
        }

        if (curMeter <= 0)
        {
            curMeter = 0;
            StopAllCoroutines();
            if (virusAlert != null)
                virusAlert.SetActive(false);
        }
    }

    public void SwitchForAnim()
    {
        if (curMeter <= 0)
        {
            anim.SetBool("IsAtForm1", true);
            anim.SetBool("IsAtForm2", false);
            anim.SetBool("IsAtForm3", false);
            anim.SetBool("IsAtForm4", false);
            anim.SetBool("IsAtForm5", false);
        }
        if (curMeter > 0 && curMeter < 25f)
        {
            anim.SetBool("IsAtForm2", true);
            anim.SetBool("IsAtForm1", false);
            anim.SetBool("IsAtForm3", false);
            anim.SetBool("IsAtForm4", false);
            anim.SetBool("IsAtForm5", false);
        }
        if (curMeter >= 25f && curMeter < 50f)
        {
            anim.SetBool("IsAtForm3", true);
            anim.SetBool("IsAtForm2", false);
            anim.SetBool("IsAtForm1", false);
            anim.SetBool("IsAtForm4", false);
            anim.SetBool("IsAtForm5", false);
        }
        if (curMeter >= 50f && curMeter < 75f)
        {
            anim.SetBool("IsAtForm4", true);
            anim.SetBool("IsAtForm2", false);
            anim.SetBool("IsAtForm3", false);
            anim.SetBool("IsAtForm1", false);
            anim.SetBool("IsAtForm5", false);
        }

        if (curMeter >= 100)
        {
            anim.SetBool("IsAtForm5", true);
            anim.SetBool("IsAtForm2", false);
            anim.SetBool("IsAtForm3", false);
            anim.SetBool("IsAtForm4", false);
            anim.SetBool("IsAtForm1", false);
        }
    }

    public void IncreaseMeter()
    {
        curMeter += rateOfChange * Time.deltaTime;
    }

    public void DecreaseMeter()
    {
        if (waitTimer <= 0)
        {
            curMeter -= rateOfChange * Time.deltaTime;
            if(curMeter<= 0)
            {
                curMeter = 0;
                waitTimer = waitTimeLeft;
            }
        }
        else
        {
            waitTimer -= 1 * Time.deltaTime;            
        }
    }
    public void TimerReset()
    {
        waitTimer = waitTimeLeft;
    }
    public void AddToMeter(float value)
    {
        curMeter += value;
        waitTimer = waitTimeLeft;
    }
}
