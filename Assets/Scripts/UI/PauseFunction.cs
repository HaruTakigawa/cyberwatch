﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseFunction : MonoBehaviour
{
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private Animator   anim;

    public bool isPaused;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    public void Update()
    {
        if (!isPaused)
        { 
            if (Input.GetKey(KeyCode.Escape))
            {
                Pause();
            }
        }
    }

    public void Pause()
    {
        anim.SetTrigger("pause_enter");
        anim.ResetTrigger("pause_exit");
        Cursor.visible = true;
        isPaused = true;
        StartCoroutine(PauseResumeDelay());
    }

    // Continues the game via timescale and closes pause menu
    public void Resume()
    {
        // Unfreezes the game
        Time.timeScale = 1.0f;
        anim.SetTrigger("pause_exit");
        anim.ResetTrigger("pause_enter");
        Cursor.visible = true;
        isPaused = false;
        
    }

    public void ReturnToMainMenu()
    {
        // Unfreezes the game
        Time.timeScale = 1.0f;

        Cursor.visible = true;

        // Switches to Main Menu scene
        SceneManager.LoadScene("MainMenu");

    }

    IEnumerator PauseResumeDelay()
    {
        // Provides a short delay before freezing time
        yield return new WaitForSeconds(.7f);

        if (isPaused)
        {
            Time.timeScale = 0.0f;
            StopAllCoroutines();
        }
    }
}
