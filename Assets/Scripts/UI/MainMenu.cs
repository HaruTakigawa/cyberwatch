﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Switches to main scene
    public void StartGame()
    {
        StartCoroutine(DelayedStart());
        //Time.timeScale = 1.0f;
        //SceneManager.LoadScene("Main 1");

    }

    // Closes the game
    public void ExitGame()
    {
        Application.Quit();
    }

    IEnumerator DelayedStart()
    {
        Time.timeScale = 1.0f;
        yield return new WaitForSeconds(1.5f);
        //SceneManager.LoadScene("Main 1");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
