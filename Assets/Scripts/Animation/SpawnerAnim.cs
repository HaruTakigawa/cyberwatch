﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerAnim : MonoBehaviour
{
    [SerializeField] private Animator               anim;
    [SerializeField] private SuspiciousnessMeter    susMeter;

    // Update is called once per frame
    void Update()
    {
        if (susMeter.curMeter >= susMeter.maxMeter)
            anim.SetBool("isSpawning", true);

        if (susMeter.curMeter <= 0)
            anim.SetBool("isSpawning", false);
    }
}
