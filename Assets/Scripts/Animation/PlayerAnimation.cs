﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private Animator           anim;
    [SerializeField] private PlayerController   controller;
    [SerializeField] private Blink              blink;
    [SerializeField] private Ping               ping;
    [SerializeField] private Deactivate         deactivate;
    [SerializeField] private PauseFunction      pause;
    [SerializeField] private SpriteRenderer     spriteRenderer;
    [SerializeField] private Rigidbody2D        rb;

    public bool isFacingRight;
    public bool isFacingUp;

    private Vector2 movement;

    // Update is called once per frame
    void Update()
    {
        if (pause.isPaused == false)
        {
            anim.SetBool("isMoving", controller.isMoving);
            anim.SetBool("isDashing", blink.isDashing);
            anim.SetBool("isPinging", ping.isPinging);
            anim.SetBool("isDeactivating", deactivate.isDeactivating);

            float inputX = Input.GetAxisRaw("Horizontal");
            float inputY = Input.GetAxisRaw("Vertical");

            movement = new Vector2(inputX, inputY);

            // Facing
            if (Input.GetAxisRaw("Horizontal") != 0 && Input.GetAxisRaw("Vertical") != 0)
            {
                // Up Left
                if (movement.y == 1 && movement.x == -1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, -125f);
                }

                // Up Right
                if (movement.y == 1 && movement.x == 1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, 125f);
                }

                // Down Left
                if (movement.y == -1 && movement.x == -1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, -45f);
                }

                // Down Right
                if (movement.y == -1 && movement.x == 1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, 45f);
                }
            }

            else
            {
                // Left
                if (movement.x == -1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, -90f);
                }

                // Right
                if (movement.x == 1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, 90f);
                }

                // Up
                if (movement.y == 1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, 180f);
                }

                // Down
                if (movement.y == -1)
                {
                    transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
            }
        }
    }
}
