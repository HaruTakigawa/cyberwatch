﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardAnimation : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private SpriteRenderer spriteRenderer;
    //[SerializeField] private Rigidbody2D rb;
    [SerializeField] private GuardsBehaviour guard;

    private bool isMoving;
    private Vector2 movement;

    private void Start()
    {
        if (guard == null)
            guard = this.GetComponentInParent<GuardsBehaviour>();
    }
    // Update is called once per frame
    void Update()
    {
        if (guard == null) return;
        if (guard.guardState == GuardState.Idle)
        {
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsRunning", false);
        }

        if (guard.guardState == GuardState.Patrol)
        {
            anim.SetBool("IsWalking", true);
            anim.SetBool("IsRunning", true);
        }

        if (guard.guardState == GuardState.Chase)
        {
            anim.SetBool("IsRunning", true);
            anim.SetBool("IsWalking", true);
        }

        if (guard.guardState == GuardState.Wander)
        {
            anim.SetBool("IsWalking", true);
            anim.SetBool("IsRunning", true);
        }

        if (guard.guardState == GuardState.Deactivated)
        {
            anim.SetBool("IsWalking", false);
            anim.SetBool("IsRunning", false);
        }

        if (guard.guardState == GuardState.Pinged)
        {
            anim.SetBool("IsWalking", true);
            anim.SetBool("IsRunning", true);
        }
    }
}
